from circuits import (
	Loader, Debugger,
	Component, Worker,
	Event, handler
)

import sys, os

for i, (it) in enumerate([
	Component, Event, Worker, handler,
]):
	try:
		__builtins__[it.__qualname__] = it
	except TypeError:
		setattr(__builtins__, it.__qualname__, it)


class Application(Component):
	channel = "Application"
	
	loader = Loader(
		paths = sys.path[0],
		auto_register = False
	)
	
	#@handler('event', "load_done", channel='*')
	def started(self, component):
		
		result = self.call(Event.create("load", "Component"),
			"loader")
		
		list(result)

(Debugger() + Application( )).run()
